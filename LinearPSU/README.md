USE AT YOUR OWN RISK!

This is my PSU design for my own Eurorack and I've assumed the risk. I'm
just sharing this so folks can see the designs. Don't use this design
unless you know what you're doing. Even then, probably still don't
use this design. If you do, you assume sole responsibility.

https://docs.google.com/spreadsheets/d/1vNxJUNZ4Abzbb8N3QlJ8hspJkiFJsFDDUHU5n0iTpqY/edit#gid=0

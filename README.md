# BitByBit Synths Gave

A chiptune Eurorack digital oscillator which is inspired by the
GameBoy WAVE channel and uses the ElectroSmith Daisy platform
(either the Seed or Submodule, TBD).

This project is open source / open hardware with the aim that
folks can build this on their own. I also plan on selling completed
modules and kits (via [BitByBit Synths](https://www.bitbybitsynths.com).
Keep this in mind if you decide you might like to contribute, though
contributions are welcome!

Specifically the software is avilable under the GPLv3 license, and
the hardware designs under the Creative Commons Attribution-ShareAlike 3.0 
Unported license.

## Overview

The GameBoy WAVE channel is a 4-bit 32-entry wavetable. On the GameBoy,
the wavetable can be updated. For instance in LSDJ you can automatically
or manually flip frames.

For this module, however, at least the first version, it is only going to
be a static waveform which you can update manually using buttons and
see using some sort of display (likely the common OLED display or maybe eInk).

The wave is intended to be a single cycle. Initially only a single wave will
be supported, which will be stored in flash.

## High Level Approach

The GameBoy appears to have used countdown timers to control the speed it would
march through the 32-slot table. High frequencies meant the timer would fire
more quickly and, thus, it would sample the table faster.

The current idea is to do something similar where we adjust the sampling rate
depending on what the pitch is. For example, a 32-slot wave table at a
desired pitch of 440 (Middle-A) would require a sample rate of 14.08Khz.

One problem with this is that the Daisy doesn't appear to let you change
the sampling rate of the audio DAC beyond fixed settings (e.g. 22, 44, 96Khz).
It may be possible to override perhaps. Another option is to use one of the CV
Outputs, which appears to use a 12-bit DAC (0-4095 steps mapped to 0-5V). That
requires a bit more hardware to bump the levels up to Eurorack standard
(-5 to +5V) but vastly simplifies the audio portion of the code and avoids
interpolation and other complicated concepts which aren't necessary for an
intentionally lo-fi chiptune synth such as this one.

What remains to be seen is how fast the CV Output can be updated as higher
pitches would go pretty far into the audio range.

## Minimal Control Surface

This is the absolute minimum to get things going though given Daisy is super
powerful, future options could be considered.

### CV Inputs:

  * V/Oct

### Buttons

  * 4 Tactile Switches (in a plus, much like the GameBoy)
        This is how one will draw the waveform

### Pots

  * Coarse Tune
  * Fine Tune

## Possible Future Enhancements

The initial module is intended to be simplistic by design. However some
future options could be useful down the road for something a bit more
flexible/complete which could also mean a standalone (non-Eurorack)
synthesizer might be possible.

### Init Patches

Some pre-defined patches to start from so one doesn't have to draw
from scratch would be useful.

### Wavetable bank(s)

LSDJ supports a small bank of wavetables that are associated with
an instrument/patch. These can be controlled manually or
automatically stepped through. Both could be something that could
be supported. External stepping could use a gate or perhaps CV
and/or just a pot.

Implementing this would make the Gave more feature-complete to LSDJ.
In the spirit of chiptune, though, a morphing wavetable is likely
out of scope since there are already plenty of good wavetables
(Osiris notably, which itself uses the Daisy Seed) and LSDJ doesn't
do any fancy morphing.

Having banks also lets you save waves you like to be used later
without having to draw them in.

### Higher bitrate waveforms

The ability to select higher bit-depth/entry wavetable. For using the
D-pad style control surface, this breaks down pretty quickly past say
8-bits and might require an external application or bigger control
surface.

### Menu Divey Control Surface

At the cost of menu-diving, adding some options like an internal virtual
envelope and VCA could be useful. This requires adding a Gate input.

### D-pad and Buttons with a Performance Mode

Thanks to heckseven on the EMS Discord for this idea. To pay homage to the
GB, could setup a D-pad and buttons and arcade buttons and enable a
performance mode which would do fun things like transpose up/down an
octave, perhaps an arp, swap wavetables, etc.

## Links

  * https://github.com/ManojBR105/Teensy-4.0_digital_parallel_read-write
  * https://www.youtube.com/watch?v=IDrWtgTb3D4
  * https://www.falstad.com/circuit/index.html
  * https://www.electro-smith.com/daisy
  * https://electro-smith.github.io/libDaisy/classdaisy_1_1_timer_handle.html 
  * https://gbdev.gg8.se/wiki/articles/Gameboy_sound_hardware#Wave_Channel
  * https://www.waveshare.com/epaper?dir=asc&order=price
  * https://www.adafruit.com/product/4868#technical-details
  * https://docs.google.com/spreadsheets/d/1-CbMzUJy2_KEaQ1NSyPtlGTLqVAiWgYpT_GnH3tR4Kw/edit?usp=sharing
  * https://www.arduino.cc/reference/en/libraries/u8g2/ OLED/eInk Library
  * https://www.youtube.com/watch?v=wqS93Cgjvyw

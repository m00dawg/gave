#include "patches.h"

// SD Card File I/O
SdFs sd;
FsFile patch_file;

// Pre-allocate space for patches in RAM
Patch patches[NUM_PATCHES];

// Current patch
byte current_patch = 0;
// Current wave in patch
byte current_wave = 0;
// Current step of the wave to output for the step-sequencer
byte current_step = 0;

// RAM buffer for scaled waves from patch
volatile uint16_t scaled_waves[WAVE_FRAMES][WAVE_SLOTS];

// Scale the current wave for the current patch up to the AUDIO_DAC resolution
// by bit-shifting and placing in our waves buffer.
void scaleWave(byte wave_number) {
  for (byte i = 0; i < WAVE_SLOTS; i++) {
    scaled_waves[wave_number][i] = map(patches[current_patch].frames[wave_number][i], 0, WAVE_MAX, 0, AUDIO_DAC_MAX);
  }
}

// Scale all waves in current patch up to AUDIO_DAC resolution. Generally should only
// need to call this if we switch patches.
void scaleWaves() {
  for (byte i = 0; i < WAVE_FRAMES; i++) {
    scaleWave(i);
  }
}

// Load all patches from the SD card into RAM
void loadAllPatches() {
  char c;
  Serial.println("Loading all patches");
  
  patch_file = sd.open(PATCHES_FILE_NAME, FILE_READ);
  if (!patch_file) {
    Serial.println("Error, failed to open file reading!");
    while(1) yield();
  }

  while (patch_file.available()) {
    // Iterate through each patch
    for (byte patch_count = 0; patch_count < NUM_PATCHES ; patch_count++) {

      // Read the patch name
      for (byte name_count = 0 ; name_count < PATCH_NAME_LENGTH; name_count++) {
        c = patch_file.read();
        patches[patch_count].name[name_count] = c;
        Serial.print(c);
      }
      Serial.println();
      //patches[patch_count].name[PATCH_NAME_LENGTH];
      // Read Newline
      patch_file.read();
     
      // Then the waves
      for (byte wave_frame_count = 0 ; wave_frame_count < WAVE_FRAMES ; wave_frame_count++) {
        Serial.print("Loading Frame: ");
        Serial.println(wave_frame_count);
        Serial.print("Wave: ");
        for (byte wave_slot_count = 0 ; wave_slot_count < WAVE_SLOTS ; wave_slot_count++) {
          c = patch_file.read();
          patches[patch_count].frames[wave_frame_count][wave_slot_count] = charToHex(c);
          Serial.print(c);
        }
        Serial.println();
        // Read Newline
        patch_file.read();
      }
    }
  }
  patch_file.close();
  scaleWaves();
}

// Save all patches from RAM to the SD card
// This is less refined, ideally saving a single patch might make more sense
// from the standpoint of wear, but this is what we got for now.
void saveAllPatches() {
  // Erase exiting patch file
  Serial.println("Erasing Patch File");
  if (!sd.remove(PATCHES_FILE_NAME)) {
    Serial.println("Error, couldn't delete file!");
    while(1) yield();
  }
  
  // Open file for writing
  patch_file = sd.open(PATCHES_FILE_NAME, FILE_WRITE);
  if (!patch_file) {
    Serial.println("Error, failed to open file for writing!");
    while(1) yield();
  }
  Serial.print("Opened file ");
  Serial.print(PATCHES_FILE_NAME);
  Serial.println (" for writing...");

  delay(1000);

  // Iterate through patches
  for (byte patch_count = 0 ; patch_count < NUM_PATCHES ; patch_count++)
  {
    Serial.print("Writing patch ");
 
    // Write the patch name, one byte at a time
    // Doesn't seem to work otherwise, probably due to lack of null string
    for (byte name_count = 0 ; name_count < PATCH_NAME_LENGTH; name_count++) {
      Serial.print(patches[patch_count].name[name_count]);
      patch_file.write(patches[patch_count].name[name_count]);    
    }
    Serial.println();
    patch_file.write("\n");
         
   // Iterate through waves per patch
   for (byte frame_count = 0 ; frame_count < WAVE_FRAMES ; frame_count++)
   {
     Serial.print("Writing wave frame ");
     Serial.println(frame_count);
     for (byte slot_count = 0 ; slot_count < WAVE_SLOTS ; slot_count++) {
      Serial.print(hexToChar(patches[patch_count].frames[frame_count][slot_count]));
      patch_file.write(hexToChar(patches[patch_count].frames[frame_count][slot_count]));
     }
     Serial.println();
     patch_file.write("\n");
   }
  }
  patch_file.close();
}

// Convert hex character to int
// This is bad and I feel bad.
byte charToHex(char chr) {
  switch (chr) {
    case '0':
      return 0;
    case '1':
      return 1;
    case '2':
      return 2;
    case '3':
      return 3;
    case '4':
      return 4;
    case '5':
      return 5;
    case '6':
      return 6;
    case '7':
      return 7;
    case '8':
      return 8;
    case '9':
      return 9;
    case 'a':
      return 10;
    case 'b':
      return 11;
    case 'c':
      return 12;
    case 'd':
      return 13;
    case 'e':
      return 14;
    case 'f':
      return 15;
    default:
      return 0;
  }
}

// Convert int to hex character
char hexToChar(int value) {
  return (char) String(value, HEX)[0];
}

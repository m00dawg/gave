// Audio Stuff

// Tuning to adjust ADC
struct Tuning {
  int adc_value;
  float adjustment;
};

static Tuning tunings[] = 
  {
    { 802, 1.05},
    {1660, 1.0},
    {2480, 1.0},
    {3310, 1.0},
    {4095, 1.0}
  };


/* Calculate the pitch based on V/Oct and Tune using the MIDI formula which is:
 *  
 *  pitch = 440 * 2^((note - 69) / 12)
 *  
 *  And can be found here:
 *  https://www.inspiredacoustics.com/en/MIDI_note_numbers_and_center_frequencies
 */
void updatePitch()
{
  
  // For when we have something to tune, but I can't seem to make this work 
  // Section to better adjust pitch
  int raw_adc = analogRead(VOCT);
  /*
  adj_value = 1;
  for (int count = 0; count < sizeof(tunings); count++)
  {
    if(raw_adc < tunings[count].adc_value) {
      adj_value = tunings[count].adjustment;
      break;
    }
  } 
  */
  voct_note = (map(raw_adc, ADC_MAX, 0, 0, ADC_MAX)) / float(VOCT_STEPS_PER_NOTE) * tuning_adj_value;
  
  // The value of the ADC needs to be inverted due to using a negative feedback op-amp curciut
  //voct_note = (map(analogRead(VOCT), ADC_MAX, 0, 0, ADC_MAX)) / float(VOCT_STEPS_PER_NOTE);
  
  // Use the tune knob to adjust pitch up from base
  tune_note = analogRead(TUNE) / float(TUNE_RESOLUTION);

  // Finally generate the pitch using the magical formula and multiply it by the number of
  // wave slots to get our actual sample rate.
  pitch = 440 * pow(2,(voct_note + tune_note - 69) / 12);
  sample_rate = WAVE_SLOTS * pitch;
}

// Callback to update the wave index when our timer expires,
// output the new value to the AUDIO_DAC, and reset the
// timer for the desired pitch.
void waveStepCallback() {
  noInterrupts();

  // Step through our wave index, wrapping around when we reach the end.
  dac_wave_index = ++dac_wave_index % WAVE_SLOTS;

  // Magic writing to the DAC
  // See https://github.com/adafruit/Adafruit_ZeroTimer/blob/master/examples/timers_callback/timers_callback.ino
  while (DAC->SYNCBUSY.bit.DATA0);
  DAC->DATA[0].reg = scaled_waves[current_wave][dac_wave_index];

  // Reset timer to new sample rate
  TC.setPeriod(MICROSECONDS / sample_rate);

  interrupts();
}

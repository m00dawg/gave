FsFile config_file;

void loadConfig() {
  config_file = sd.open(CONFIG_FILE_NAME, FILE_READ);
  if (config_file.available()) {
    //String buffer = config_file.readStringUntil('\n');
    //tuning_adj_value = buffer.toFloat();
    tuning_adj_value = config_file.parseFloat();
    Serial.print("Reading tuning value ");
    Serial.println(tuning_adj_value, 6);
  }
  config_file.close();
}

void saveConfig() {
  Serial.print("Saving Config...");
  int return_value;

  Serial.println("Erasing Config File");
  if (!sd.remove(CONFIG_FILE_NAME)) {
    Serial.println("Error, couldn't delete file!");
    while(1) yield();
  }

  config_file = sd.open(CONFIG_FILE_NAME, FILE_WRITE);
  if (!config_file) {
    Serial.println("Error, failed to open config file for writing!");
    while(1) yield();
  }
  Serial.print("Writing tuning value ");
  Serial.println(tuning_adj_value, 6);
  delay(1000);
  return_value = config_file.println(tuning_adj_value, 6);
  config_file.close();
  Serial.println("Done!");
  Serial.print("Bytes: ");
  Serial.println(return_value);
}

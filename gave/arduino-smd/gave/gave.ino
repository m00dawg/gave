// Title: Gave (SMD Version)
// Description: Implements a GameBoy WAVE style 4-bit/32 slot single wavetable
// Hardware: Adafruit ItsyBitsy M4
// Author: Tim Soderstrom

/* 
 *  Links:
 * https://learn.adafruit.com/introducing-adafruit-itsybitsy-m4/pinouts
 * https://github.com/ManojBR105/Teensy-4.0_digital_parallel_read-write
 * https://www.falstad.com/circuit/index.html
 * https://www.inspireAUDIO_DACoustics.com/en/MIDI_note_numbers_and_center_frequencies
 */

/* Pins Used:
 * See defines.h
 * 
 *  Pin A0: Audio (AUDIO_DAC)
 *  Pin A1: Step (AUDIO_DAC2)
 *  Pin A2: V/Oct
 *  Pin A3: Frame
 *  Pin A4: Tune
 *  Pin A5: Frame Trigger
 *  
 *  Pin 02:  SD Card (DAT3/CD, Chip-Select)
 *  Pin SCK: SD Card
 *  Pin MO:  SD Card
 *  Pin MI:  SD Card
 *  
 *  Pin 6:    RGB LED Clock
 *  Pin 8:    RGB LED Data
 *  Pin 7:

 #define ENCODER_UP 9
#define ENCODER_DOWN 7
#define ENCODER_LEFT 12
#define ENCODER_RIGHT 13
#define BUTTON_MENU 10
#define BUTTON_BACK 11
 *  
 *  Pin SDA: OLED SDA
 *  Pin SDL: OLED SDL
 *  

 */
 
// INCLUDES

#include <Arduino.h>
// Without this USB Serial doesn't seem to work right
// #include <Adafruit_TinyUSB.h>
// Lib for turning off that awful RGB LED
#include <Adafruit_DotStar.h>
// Display Libs
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <SdFat.h>

// Interrupts
// This library caused pitch issues for some reason
//#include <SAMDTimerInterrupt.h>
// This library does not.
#include "SAMD51_InterruptTimer.h"

// Encoders & Buttons
#include <SimpleRotary.h>

// DEFINES
#include "defines.h"

// GLOBAL VARIABLES

// Display Variables
int static pixel_size_x = OLED_SCREEN_X / WAVE_SLOTS - 1;
int static pixel_size_y = OLED_SCREEN_Y / WAVE_MAX - 1;

// Raw note from V/Oct
float voct_note = 0;
// Raw note from tune
float tune_note = 0;
// Pitch, ultimately coming from v/oct plus tune
float pitch = 110;

// Naive tuning for the DAC. Sending a flaot to serial in changes this value.
// This seems to be closest at a -3 cents split.
float tuning_adj_value = 1.009811;

// Frame CV Value
int frame_cv = 0;

// Current sample rate to run the AUDIO_DAC at
// (pitch * 32 since there are 32 wave steps)
float sample_rate = 14080;  // Default to Middle-A

// Track where we are in the wave table for the DAC
volatile byte dac_wave_index = 0;

// Track where the user is in the wave index
byte user_wave_index = 0;

// For serial output debugging mostly
int prevMillis = 0;

// Flag to indicate if there was a trigger event
bool trigger = false;

// OBJECTS
// Display
Adafruit_SSD1306 display(OLED_SCREEN_X, OLED_SCREEN_Y, &Wire, OLED_RESET);

// That horrendously bright LED
Adafruit_DotStar rgbled(1, RGB_LED_DATAPIN, RGB_LED_CLOCKPIN, DOTSTAR_BRG);

// Control Surface
// DailyStruggleButton buttonMenu;
// DailyStruggleButton buttonBack;
SimpleRotary upDownEncoder(ENCODER_UP, ENCODER_DOWN, BUTTON_MENU);
SimpleRotary leftRightEncoder(ENCODER_RIGHT, ENCODER_LEFT, BUTTON_BACK);

// Local Includes
#include "patches.h"

// FUNCTIONS
void setup() {  
  Serial.begin(9600);
  delay(5000);
  
  Serial.println("Setup Pins");
  
  // Pins   
  pinMode(VOCT, INPUT_PULLUP);
  pinMode(TUNE, INPUT_PULLUP);
  pinMode(TRIGGER, INPUT_PULLUP);

   // Switch Encoders 
  leftRightEncoder.setDebounceDelay(ENCODER_DEBOUNCE);
  leftRightEncoder.setErrorDelay(ENCODER_ERROR_DELAY);
  upDownEncoder.setDebounceDelay(ENCODER_DEBOUNCE);
  upDownEncoder.setErrorDelay(ENCODER_ERROR_DELAY);
  
  // Turn off the freakishly bright RGB LED
  rgbled.begin();
  rgbled.show(); // Initialize all pixels to 'off'

  // DAC Setup
  Serial.println("Setup DAC");
  // Set DAC resolution
  analogWriteResolution(DAC_RESOLUTION);
  analogReadResolution(ADC_RESOLUTION);
  
  // Magic https://github.com/adafruit/Adafruit_ZeroTimer/blob/master/examples/timers_callback/timers_callback.ino
  analogWrite(AUDIO_DAC, 128);
  analogWrite(STEP_DAC, 128);

  // Init SD Card
  sd.begin(SdSpiConfig(SD_CARD, SHARED_SPI, SD_SCK_MHZ(16)));

  // Load config
  loadConfig();
  
  // Load all patches into RAM
  loadAllPatches();
  

  // Setup the display
  Serial.println("Init Display");
  initDisplay();

  // Start our main audio callback
  Serial.println("Enable Wave Callback");
  TC.startTimer(MICROSECONDS / sample_rate, waveStepCallback);
  
  Serial.println("Setup Complete");
}

void loop() {
  // Read Frame CV, update wave is required
  checkWaveStep();
  // Read V/Oct and Tune knob and set sample_rate
  updatePitch();
  updateStep();
  pollControlSurface();
  checkDisplay();
  //debugPrintVOct();
  
  // Routine to set tune adjustment, but this should be in a menu
  /*
  if (Serial.available() > 0) {
    tuning_adj_value = Serial.parseFloat();
    Serial.print("New adj: ");
    Serial.println(adj_value);
  }
  */
}

void debugPrintVOct() {
  if(prevMillis + 1000 < millis())
  {
    Serial.print("Note: ");
    Serial.println(voct_note);
    Serial.print("Tune Note: ");
    Serial.println(tune_note);
    Serial.print("Effective Note: ");
    Serial.println(voct_note + tune_note);
    Serial.print("Pitch: ");
    Serial.println(pitch);
    Serial.print("Sample Rate: ");
    Serial.println(sample_rate);
    Serial.print("Raw V/Oct Value: ");
    Serial.println(ADC_MAX - analogRead(VOCT));
    Serial.print("Raw ADC Value: ");
    Serial.println(analogRead(VOCT));
    Serial.print("tuning_adj_value: ");
    Serial.println(tuning_adj_value);
    
    //Serial.print("Adjusted V/Oct Value: ");
    //Serial.println(adj_value);
    prevMillis = millis();  
  }   
}

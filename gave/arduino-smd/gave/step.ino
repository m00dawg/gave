  // Step sequencer

// Soon as we see a trigger, we need to advance the step
// but should only do so once per trigger
// (if trigger is held high, only step once)

void updateStep() {
  // If we read a trigger (it goes low), and the trigger flag was not already true,
  // set the trigger flag and update the step.
  if (!digitalRead(TRIGGER) && !trigger)
  {
    Serial.println("Trigger!");
    trigger = true;
    // Step through our wave index, wrapping around when we reach the end.
    current_step = ++current_step % WAVE_SLOTS;

    // Magic writing to the DAC
    // See https://github.com/adafruit/Adafruit_ZeroTimer/blob/master/examples/timers_callback/timers_callback.ino
    while (DAC->SYNCBUSY.bit.DATA1);
    DAC->DATA[1].reg = scaled_waves[current_wave][current_step];

    // Signal we need to refresh the display to increment the step value
    updateDisplay = true;
  }
  
  // If trigger is released (it goes high) and our flag was true, unset flag
  else if (digitalRead(TRIGGER) && trigger) {
    Serial.println("Untrigger!");
    trigger = false;
  }
  
}

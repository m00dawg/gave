#pragma once
#include <Arduino.h>
#include <SdFat.h>
#include "defines.h"

// Struct for a single patch
struct Patch {
  char name[PATCH_NAME_LENGTH];
  char stepDirection;
  byte frames[WAVE_FRAMES][WAVE_SLOTS];
};

class Patches {
  private:
    SdFs* sd;
    FsFile patchFile;

    // Convert hex character to int
    byte charToHex(char chr);

    // Convert int to hex character
    char hexToChar(int value);

    int frameCV = 0;

  public:
    volatile uint16_t scaledWaves[WAVE_FRAMES][WAVE_SLOTS];
    Patch patches[NUM_PATCHES];
    byte currentPatch = 0;
    byte currentWave = 0;
    
    // Constructor
    Patches(SdFs* sd);

    // Scale the current wave for the current patch up to the AUDIO_DAC resolution 
    // by bit-shifting and placing in our waves buffer.
    void scaleWave(byte waveNumber);

    // Scale all waves in current patch up to AUDIO_DAC resolution. Generally should only
    // need to call this if we switch patches.
    void scaleWaves();

    // Return the current selected patch as a struct
    Patch getCurrentPatch();

    // Evaluate CV input and see if we need to update the wave-frame
    void updateWaveFrame();
  
    // Load all patches from flash into RAM
    void loadAllPatches();

    // Save all patches from RAM to flash
    void saveAllPatches();

    // Update the current patch's step direction
    void updateStepDirection(char direction);

    // Initialize/Begin
    void begin();
};



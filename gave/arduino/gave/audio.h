// Audio Stuff
#include <Arduino.h>
#include "SAMD51_InterruptTimer.h"
#include "patches.h"
#include "config.h"


class Audio {
  private:
    Config* config;
    Patches* patches;
    
    int voctADC = 0;
    float voctNote = 0;
    float tuneNote = 0;
    float pitch = 0;

  public:
    float sampleRate;

    // Constructor
    Audio(Config* config);

    void updatePitch();
};
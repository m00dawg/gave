// Pins
#define SD_CARD 2
#define RGB_LED_CLOCKPIN   6
#define RGB_LED_DATAPIN    8
#define ENCODER_UP 9
#define ENCODER_DOWN 7
#define ENCODER_LEFT 12
#define ENCODER_RIGHT 13
#define BUTTON_MENU 10
#define BUTTON_BACK 11

#define AUDIO_DAC A0
#define STEP_DAC A1
#define VOCT A2
#define FRAME A3
#define TUNE A4
#define TRIGGER A5


// Devices
#define OLED_SCREEN_X   128
#define OLED_SCREEN_Y   64
#define OLED_RESET      -1    // No reset pin
#define SCREEN_ADDRESS  0x3C  // Found on the device

// Waves
#define WAVE_SLOTS        32            // Number of samples per wave
#define WAVE_RESOLUTION   4             // Wave bits (4-bit)
#define WAVE_MAX          0xF           // Max wave range (4-bit)
#define WAVE_FRAMES       16            // Number of Frames (waves) per patch
#define NUM_PATCHES       16            // Number of Patches
#define PATCH_NAME_LENGTH 16            // Max name of patch (limited by display)
#define PATCHES_FILE_NAME "patches.txt" // Filename for patch storage in SPI flash
#define PATCH_BYTES       528           // Number of bytes per patch in patch file, PATCH_NAME_LENGTH + (WAVE_FRAMES * WAVE_SLOTS)
#define PATCH_CHAR_MIN    32            // Min ASCII value we allow in a patch name
#define PATCH_CHAR_MAX    122           // Max ASCII value we allow in a patch name

// Config
#define CONFIG_FILE_NAME "config.txt"   // Filename for patch storage in SPI flash

// Audio
#define DAC_RESOLUTION        12      // 12-bit
#define AUDIO_DAC_MAX         4095    // 12-bit
#define AUDIO_DAC_INIT        128     // Magic # to init the DAC for fancy writes, I think?  
#define ADC_RESOLUTION        12      // 12-bit
#define ADC_MAX               4095    // 12-bit
#define TUNE_RESOLUTION       170     // 2 Octaves
#define VOCT_NOTE_RANGE       60      // 5 octaves (60 notes)
#define VOCT_STEPS_PER_NOTE   68.2666 // How many ADC steps per note

// Frame CV
// Need to add an option to make this bipolar
#define FRAME_ADC_MAX         2048    // Positive Only
#define FRAME_ADC_MIN          128
#define MAX_FRAME               15    // 0 to WAVE_FRAMES - 1

// Constants
#define MICROSECONDS 1000000

// Module Mappings
#define SHOW_WAVE 1
#define MAIN_MENU 2
#define BLANK 3

// Buttons
#define BUTTON_DEBOUNCE     20    //ms
#define ENCODER_DEBOUNCE    2     //ms
#define ENCODER_ERROR_DELAY 200   //ms
#define ENCODER_TURNED_LEFT 1
#define ENCODER_TURNED_RIGHT 2

// Characters
#define CHAR_SPACE          32
#define CHAR_BANG           33
#define CHAR_SLASH          48
#define CHAR_ZERO           48
#define CHAR_NINE           57
#define CHAR_COLON          58
#define CHAR_AT             64
#define CHAR_UPPER_A        65
#define CHAR_UPPER_Z        90
#define CHAR_LEFT_BRACKET   91
#define CHAR_BACKTICK       96
#define CHAR_LOWER_A        97
#define CHAR_LOWER_Z        122

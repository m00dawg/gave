/* Module which shows the main menu */
#include "mainmenu.h"

MainMenu::MainMenu(Adafruit_SSD1306* display, SimpleRotary* upDownEncoder, SimpleRotary* leftRightEncoder, Patches* patches) {
  this->display = display;
  this->upDownEncoder = upDownEncoder;
  this->leftRightEncoder = leftRightEncoder;
  this->patches = patches;
}

void MainMenu::printMenu() {
  display->clearDisplay();
  display->setCursor(0, 0);     // Start at top-left corner
  display->setTextColor(WHITE, BLACK);
  display->println("MENU\n----");
  for (int i = 0; i < menuSize; i++) {
    if(menuSelection == i)
      display->setTextColor(BLACK, WHITE);
    else
      display->setTextColor(WHITE, BLACK);
    display->println(mainMenuList[i]);
  }
  display->display();
}

void MainMenu::activate () {
  printMenu();
}

byte MainMenu::run() {
  udRotate = upDownEncoder->rotate();
  //myLRRotate = myLeftRightEncoder->rotate();
  if (udRotate == ENCODER_TURNED_LEFT) {
    menuSelection = --menuSelection % menuSize;
    if(menuSelection == 255)
      menuSelection = menuSize;
    printMenu();
  }
  else if (udRotate == ENCODER_TURNED_RIGHT) {
    menuSelection = ++menuSelection % menuSize;
    printMenu();
  }


  if(upDownEncoder->push()) {
    switch (menuSelection) {
      case MENU_SHOW_WAVE:
        return SHOW_WAVE;
      case MENU_NAME_PATCH:
      case MENU_LOAD_PATCH:
      case MENU_SAVE_ALL_PATCHES:
        display->clearDisplay();
        display->setCursor(0, 0);
        display->println("Saving all patches");
        display->println("to SD Card");
        display->display();
        patches->saveAllPatches();
        activate();
        return MAIN_MENU;
      case MENU_SCREENSAVER:
        return BLANK;
      case MENU_CONFIG:
      default:
        return MAIN_MENU;
    }
  }

  return MAIN_MENU;
}
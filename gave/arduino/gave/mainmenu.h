/* Module which shows the main menu */
#include <Arduino.h>
#include <Adafruit_SSD1306.h>
#include <SimpleRotary.h>
#include "defines.h"
#include "module.h"
#include "patches.h"

#define MENU_SHOW_WAVE 0
#define MENU_NAME_PATCH 1
#define MENU_LOAD_PATCH 2
#define MENU_SAVE_ALL_PATCHES 3
#define MENU_SCREENSAVER 4
#define MENU_CONFIG 5

static byte menuSize = 6;

class MainMenu: public Module {
  private:
    Patches* patches;
    String mainMenuList[6] = {
      "Show Wave",
      "Name Patch",
      "Load Patch",
      "Save All Patches",
      "Screensaver",
      "Config"
    };
    byte menuSelection = 0;
    byte lrRotate = 0;
    byte udRotate = 0;
    void printMenu();
  public:
    MainMenu(Adafruit_SSD1306* display, SimpleRotary* upDownEncoder, SimpleRotary* leftRightEncoder, Patches* patches);
    void activate();
    byte run();
};
#include "config.h"

Config::Config(SdFs* sd) { 
  this->sd = sd;
}

void Config::loadConfig() {
  configFile = sd->open(CONFIG_FILE_NAME, FILE_READ);
  if (configFile.available()) {
    //String buffer = config_file.readStringUntil('\n');
    //tuning_adj_value = buffer.toFloat();
    tuningAdjustment = configFile.parseFloat();
    //Serial.print("Reading tuning value ");
    //Serial.println(tuning_adj_value, 6);
  }
  configFile.close();
}

void Config::saveConfig() {
  //Serial.print("Saving Config...");
  //int return_value;

  //Serial.println("Erasing Config File");
  if (!sd->remove(CONFIG_FILE_NAME)) {
    Serial.println("Error, couldn't delete file!");
    while(1) yield();
  }

  configFile = sd->open(CONFIG_FILE_NAME, FILE_WRITE);
  if (!configFile) {
    Serial.println("Error, failed to open config file for writing!");
    while(1) yield();
  }
  //Serial.print("Writing tuning value ");
  //Serial.println(tuning_adj_value, 6);
  //delay(1000);
  configFile.println(tuningAdjustment, 6);
  configFile.close();
  //Serial.println("Done!");
  //Serial.print("Bytes: ");
  //Serial.println(return_value);
}

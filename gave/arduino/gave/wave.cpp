/* Module which shows the main menu */
#include "wave.h"

#define WAVE_CURSOR_Y 52
#define STEP_CURSOR_Y 48

int static pixelSizeX = OLED_SCREEN_X / WAVE_SLOTS - 1;
int static pixelSizeY = OLED_SCREEN_Y / WAVE_MAX - 1;

Wave::Wave(Adafruit_SSD1306* display, SimpleRotary* upDownEncoder, SimpleRotary* leftRightEncoder, Patches* patches, Step* step) {
  this->display = display;
  this->upDownEncoder = upDownEncoder;
  this->leftRightEncoder = leftRightEncoder;
  this->patches = patches;
  this->step = step;
}

void Wave::updateDisplay() {
  //currentPatch = patches->getCurrentPatch();
  currentWave = patches->currentWave;
  currentStep = step->currentStep;
  display->setCursor(0, 0);     // Start at top-left corner
  display->clearDisplay();
  display->printf("Fr: %02d | St: %02d %c\n", patches->currentWave, step->currentStep, step->direction);  
  drawWaveCursor();
  drawStepCursor();
  drawWave();
  display->display();
}

void Wave::updateWave() {
  // Update wave index (cursor)
  lrRotate = leftRightEncoder->rotate();
  if (lrRotate == ENCODER_TURNED_LEFT) {
    waveIndex = --waveIndex % WAVE_SLOTS;
    if(waveIndex == 255)
      waveIndex = WAVE_SLOTS;
    updateDisplay();
  }
  else if (lrRotate == ENCODER_TURNED_RIGHT) {
    waveIndex = ++waveIndex % WAVE_SLOTS;
    updateDisplay();
  }

  // Update wave value
  udRotate = upDownEncoder->rotate();
  // Up
  if (udRotate == ENCODER_TURNED_RIGHT) {
    patches->patches[patches->currentPatch].frames[currentWave][waveIndex] = 
      ++patches->patches[patches->currentPatch].frames[currentWave][waveIndex] % (WAVE_MAX + 1);
    patches->scaleWave(currentWave);
    updateDisplay();
  }
  // Down
  else if (udRotate == ENCODER_TURNED_LEFT) {
    patches->patches[patches->currentPatch].frames[currentWave][waveIndex] = 
      --patches->patches[patches->currentPatch].frames[currentWave][waveIndex] % (WAVE_MAX + 1);
    patches->scaleWave(currentWave);
    updateDisplay();
  }
}

void Wave::activate() {
  updateDisplay();
}

byte Wave::run() {
  // If something external changed, update the display
  if(currentWave != patches->currentWave || 
     currentStep != step->currentStep) {
    updateDisplay();
  }

  updateWave();

  // Pressing the LR encoder (the right one) iterates through the step modes
  if(leftRightEncoder->push()) {
    step->cycleStepDirection();
    updateDisplay();
  }

  // Pressing the UD encoder (the left one) returns to the main menu
  if(upDownEncoder->push()) {
    return MAIN_MENU;
  }
  return SHOW_WAVE;
}

// Draw pixel sized to the display scale, based on the wave size
void Wave::drawPixel(int x, int y) {
  for (byte ix = 0 ; ix < pixelSizeX; ix++)
    for (byte iy = 0 ; iy < pixelSizeY; iy++) 
      // The weird OLED_SCREEN_Y - 1 - (y_pos + y) is to invert the display
      // so "up" is actually up.
      display->drawPixel(x + ix, OLED_SCREEN_Y - 1 - (y + iy), SSD1306_WHITE);
}

// Draw a pixel based on the size of the wave
void Wave::drawWave() {
  int displayXPos = 0;
  int displayYPos = 0;
  for (byte i = 0; i < WAVE_SLOTS; i++) {
    displayXPos = i * pixelSizeX;    
    displayYPos = patches->patches[patches->currentPatch].frames[patches->currentWave][i] * pixelSizeY;
    drawPixel(displayXPos, displayYPos);
    /*
    for (int  x = 0 ; x < pixelSizeX; x++)
      for (int y = 0 ; y < pixelSizeY; y++) 
        // The weird OLED_SCREEN_Y - 1 - (y_pos + y) is to invert the display
        // so "up" is actually up.
        display->drawPixel(displayXPos + x, OLED_SCREEN_Y - 1 - (displayYPos + y), SSD1306_WHITE);
    */
  }
}

// Draw the wave cursor above the corresponding wave position
// This is the position where we can update the wave via the LR knob
void Wave::drawWaveCursor() {
  int startX = waveIndex * pixelSizeX;
  int startY = WAVE_CURSOR_Y;
  drawPixel(startX , startY);
}

// Draw the step cursor above the corresponding wave position
// This is the position of the stepped output
void Wave::drawStepCursor() {
  int startX = currentStep * pixelSizeX;
  int startY = STEP_CURSOR_Y;
  drawPixel(startX , startY);
}


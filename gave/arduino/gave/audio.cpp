#include "audio.h"

// Audio Stuff

Audio::Audio(Config* config) {
  this->config = config;
}

/* Calculate the pitch based on V/Oct and Tune using the MIDI formula which is:
 *  
 *  pitch = 440 * 2^((note - 69) / 12)
 *  
 *  And can be found here:
 *  https://www.inspiredacoustics.com/en/MIDI_note_numbers_and_center_frequencies
 */
void Audio::updatePitch() {
  // For when we have something to tune, but I can't seem to make this work 
  // Section to better adjust pitch
  int voctADC = analogRead(VOCT);

  voctNote = (map(voctADC, ADC_MAX, 0, 0, ADC_MAX)) / float(VOCT_STEPS_PER_NOTE) * config->tuningAdjustment;
  
  // Use the tune knob to adjust pitch up from base
  tuneNote = analogRead(TUNE) / float(TUNE_RESOLUTION);

  // Finally generate the pitch using the magical formula and multiply it by the number of
  // wave slots to get our actual sample rate.
  pitch = 440 * pow(2,(voctNote + tuneNote - 69) / 12);
  sampleRate = WAVE_SLOTS * pitch;
}
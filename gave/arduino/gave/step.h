// Step sequencer
#pragma once
#include <Arduino.h>
#include "patches.h"

class Step {
  private:
    Patches* patches;
    bool trigger = false;
    bool pingPongDirection = false;

  public:
    byte currentStep = 0;
    char direction = 'F';

    // Constructor
    Step(Patches* patches);

    // Check for trigger and update step sequence
    void update();

    // Set the step direction
    void setStepDirection(char direction);

    // Cycle through the step direction options and return the chosen one
    char cycleStepDirection();
};


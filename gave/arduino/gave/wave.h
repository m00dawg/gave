/* Module which shows the main menu */
#include <Arduino.h>
#include <Adafruit_SSD1306.h>
#include <SimpleRotary.h>
#include "defines.h"
#include "module.h"
#include "patches.h"
#include "step.h"

class Wave: public Module {
  private:
    Patches* patches;
    Step* step;
    //Patch currentPatch;
    int currentWave = 0;
    int currentStep = 0;
    byte waveIndex = 0;
    byte lrRotate = 0;
    byte udRotate = 0;
    void updateDisplay();
    void updateWave();
    void drawPixel(int x, int y);
    void drawWave();
    void drawWaveCursor();
    void drawStepCursor();

  public:
    Wave(Adafruit_SSD1306* display, SimpleRotary* upDownEncoder, SimpleRotary* leftRightEncoder, Patches* patches, Step* step);
    void activate();
    byte run();
};
#include "patches.h"

Patches::Patches(SdFs* sd) { 
  this->sd = sd;
}

// Scale the current wave for the current patch up to the AUDIO_DAC resolution
// by bit-shifting and placing in our waves buffer.
void Patches::scaleWave(byte waveNumber) {
  for (byte i = 0; i < WAVE_SLOTS; i++) {
    scaledWaves[waveNumber][i] = map(patches[currentPatch].frames[waveNumber][i], 0, WAVE_MAX, 0, AUDIO_DAC_MAX);
  }
}

// Scale all waves in current patch up to AUDIO_DAC resolution. Generally should only
// need to call this if we switch patches.
void Patches::scaleWaves() {
  for (byte i = 0; i < WAVE_FRAMES; i++) {
    scaleWave(i);
  }
}

Patch Patches::getCurrentPatch() {
  return patches[currentPatch];
}

void Patches::updateWaveFrame() {
  // Only grab positive CV values (need to make that configurable)
  if(analogRead(FRAME) <= FRAME_ADC_MAX) {
    frameCV = map(analogRead(FRAME), FRAME_ADC_MAX, 16, 0, MAX_FRAME);
    if(frameCV != currentWave) {
      currentWave = frameCV;
    }
  }
}

// Load all patches from the SD card into RAM
void Patches::loadAllPatches() {
  //Serial.println("Loading all patches");
  
  patchFile = sd->open(PATCHES_FILE_NAME, FILE_READ);
  if (!patchFile) {
    Serial.println("Error, failed to open file reading!");
    while(1) yield();
  }

  while (patchFile.available()) {
    // Iterate through each patch
    for (byte patch_count = 0; patch_count < NUM_PATCHES ; patch_count++) {

      // Read the patch name
      for (byte name_count = 0 ; name_count < PATCH_NAME_LENGTH; name_count++) {
        patches[patch_count].name[name_count] = patchFile.read();
      }
      Serial.println();
      //patches[patch_count].name[PATCH_NAME_LENGTH];
      // Read Newline
      patchFile.read();

      // Read the step direction
      patches[patch_count].stepDirection = patchFile.read();

      // Read Newline
      patchFile.read();
     
      // Then the waves
      for (byte wave_frame_count = 0 ; wave_frame_count < WAVE_FRAMES ; wave_frame_count++) {
        //Serial.print("Loading Frame: ");
        //Serial.println(wave_frame_count);
        //Serial.print("Wave: ");
        for (byte wave_slot_count = 0 ; wave_slot_count < WAVE_SLOTS ; wave_slot_count++) {
          patches[patch_count].frames[wave_frame_count][wave_slot_count] = charToHex(patchFile.read());
          //Serial.print(c);
        }
        //Serial.println();
        // Read Newline
        patchFile.read();
      }
    }
  }
  patchFile.close();
  scaleWaves();
}

// Save all patches from RAM to the SD card
// This is less refined, ideally saving a single patch might make more sense
// from the standpoint of wear, but this is what we got for now.
void Patches::saveAllPatches() {
  // Erase exiting patch file
  Serial.println("Erasing Patch File");
  if (!sd->remove(PATCHES_FILE_NAME)) {
    Serial.println("Error, couldn't delete file!");
    while(1) yield();
  }
  
  // Open file for writing
  patchFile = sd->open(PATCHES_FILE_NAME, FILE_WRITE);
  if (!patchFile) {
    Serial.println("Error, failed to open file for writing!");
    while(1) yield();
  }
  /*
  Serial.print("Opened file ");
  Serial.print(PATCHES_FILE_NAME);
  Serial.println (" for writing...");

  delay(1000);
  */

  // Iterate through patches
  for (byte patch_count = 0 ; patch_count < NUM_PATCHES ; patch_count++)
  {
    //Serial.print("Writing patch ");
 
    // Write the patch name, one byte at a time
    // Doesn't seem to work otherwise, probably due to lack of null string
    for (byte name_count = 0 ; name_count < PATCH_NAME_LENGTH; name_count++) {
      //Serial.print(patches[patch_count].name[name_count]);
      patchFile.write(patches[patch_count].name[name_count]);    
    }
    //Serial.println();
    patchFile.write("\n");

    // Write the default step direction for the patch
    //Serial.println(patches[patch_count].stepDirection);
    patchFile.write(patches[patch_count].stepDirection);
    patchFile.write("\n");

   // Iterate through waves per patch
   for (byte frame_count = 0 ; frame_count < WAVE_FRAMES ; frame_count++)
   {
     //Serial.print("Writing wave frame ");
     //Serial.println(frame_count);
     for (byte slot_count = 0 ; slot_count < WAVE_SLOTS ; slot_count++) {
      //Serial.print(hexToChar(patches[patch_count].frames[frame_count][slot_count]));
      patchFile.write(hexToChar(patches[patch_count].frames[frame_count][slot_count]));
     }
     //Serial.println();
     patchFile.write("\n");
   }
  }
  patchFile.close();
}

// Update current patch's step direction
void Patches::updateStepDirection(char direction) {
  patches[currentPatch].stepDirection = direction;
}

void Patches::begin() {
  loadAllPatches();
  scaleWaves();
}


// Convert hex character to int
// This is bad and I feel bad.
byte Patches::charToHex(char chr) {
  switch (chr) {
    case '0':
      return 0;
    case '1':
      return 1;
    case '2':
      return 2;
    case '3':
      return 3;
    case '4':
      return 4;
    case '5':
      return 5;
    case '6':
      return 6;
    case '7':
      return 7;
    case '8':
      return 8;
    case '9':
      return 9;
    case 'a':
      return 10;
    case 'b':
      return 11;
    case 'c':
      return 12;
    case 'd':
      return 13;
    case 'e':
      return 14;
    case 'f':
      return 15;
    default:
      return 0;
  }
}

// Convert int to hex character
char Patches::hexToChar(int value) {
  return (char) String(value, HEX)[0];
}

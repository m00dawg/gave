
/* Load and save configuration options from the file on the SD card */
#pragma once
#include <Arduino.h>
#include <SdFat.h>
#include "defines.h"

class Config {
  private:
    SdFs* sd;
    FsFile configFile;
  
  public:
    float tuningAdjustment = 1.009811;
    
    // Constructor
    Config(SdFs* sd);

    void loadConfig();
    void saveConfig();
};
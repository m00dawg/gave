#pragma once
///* Module which just blanks the display */
#include <Arduino.h>
#include <Adafruit_SSD1306.h>
#include <SimpleRotary.h>
#include "defines.h"

class Module {
  protected:
    Adafruit_SSD1306* display;
    SimpleRotary* upDownEncoder;
    SimpleRotary* leftRightEncoder;
  public:
    virtual void activate();
    virtual byte run();
};
// Title: Gave (SMD/OOP Version)
// Description: Implements a GameBoy WAVE style 4-bit/32 slot single wavetable
// Hardware: Adafruit ItsyBitsy M4
// Author: Tim Soderstrom

/* 
 *  Links:
 * https://learn.adafruit.com/introducing-adafruit-itsybitsy-m4/pinouts
 * https://github.com/ManojBR105/Teensy-4.0_digital_parallel_read-write
 * https://www.falstad.com/circuit/index.html
 * https://www.inspireAUDIO_DACoustics.com/en/MIDI_note_numbers_and_center_frequencies
 */

/* Pins Used:
 * See defines.h
 * 
 *  Pin A0: Audio (AUDIO_DAC)
 *  Pin A1: Step (AUDIO_DAC2)
 *  Pin A2: V/Oct
 *  Pin A3: Frame
 *  Pin A4: Tune
 *  Pin A5: Frame Trigger
 *  
 *  Pin 02:  SD Card (DAT3/CD, Chip-Select)
 *  Pin SCK: SD Card
 *  Pin MO:  SD Card
 *  Pin MI:  SD Card
 *  
 *  Pin 6:    RGB LED Clock
 *  Pin 8:    RGB LED Data
 *  Pin 7:
 *  
 *  Pin SDA: OLED SDA
 *  Pin SDL: OLED SDL
 *  

 */
 
// INCLUDES

#include <Arduino.h>
// Without this USB Serial doesn't seem to work right
// #include <Adafruit_TinyUSB.h>
// Lib for turning off that awful RGB LED
#include <Adafruit_DotStar.h>
// Display Libs
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

// File Storage
#include <SdFat.h>

// Interrupts
// This library does not.
#include "SAMD51_InterruptTimer.h"

// Encoders & Buttons
#include <SimpleRotary.h>

// DEFINES
#include "defines.h"

// Local Includes
#include "patches.h"
#include "config.h"
#include "step.h"
#include "audio.h"
// UI Modules
#include "module.h"
#include "blank.h"
#include "mainmenu.h"
#include "wave.h"



// GLOBAL VARIABLES

// For serial output debugging mostly
int prevMillis = 0;

// Byte to track which module we are in
byte currentModuleId = 0;
// Module id the run operation of the current module returned
byte returnedModuleId = 0;

// Index of the wave as we output it through the DAC
volatile byte dacWaveIndex;

// OBJECTS
// Display
Adafruit_SSD1306 display(OLED_SCREEN_X, OLED_SCREEN_Y, &Wire, OLED_RESET);

// That horrendously bright LED
Adafruit_DotStar rgbled(1, RGB_LED_DATAPIN, RGB_LED_CLOCKPIN, DOTSTAR_BRG);

// Control Surface
SimpleRotary upDownEncoder(ENCODER_UP, ENCODER_DOWN, BUTTON_MENU);
SimpleRotary leftRightEncoder(ENCODER_RIGHT, ENCODER_LEFT, BUTTON_BACK);

// SD Card
SdFs sd;

// Local Objects
Patches patches(&sd);
Config config(&sd);
Audio audio(&config);
Step step(&patches);
// Pointer to store the current activated module
Module* currentModule;
// Instantiate modules
Blank blank(&display, &upDownEncoder, &leftRightEncoder);
MainMenu mainMenu(&display, &upDownEncoder, &leftRightEncoder, &patches);
Wave wave(&display, &upDownEncoder, &leftRightEncoder, &patches, &step);


// FUNCTIONS
void setup() {  
  Serial.begin(9600);
  
  //Serial.println("Setup Pins");
  
  // Pins   
  pinMode(VOCT, INPUT_PULLUP);
  pinMode(TUNE, INPUT_PULLUP);
  pinMode(TRIGGER, INPUT_PULLUP);

   // Switch Encoders 
  leftRightEncoder.setDebounceDelay(ENCODER_DEBOUNCE);
  leftRightEncoder.setErrorDelay(ENCODER_ERROR_DELAY);
  upDownEncoder.setDebounceDelay(ENCODER_DEBOUNCE);
  upDownEncoder.setErrorDelay(ENCODER_ERROR_DELAY);
  
  // Turn off the freakishly bright RGB LED
  rgbled.begin();
  rgbled.show(); // Initialize all pixels to 'off'

  // DAC Setup
  //Serial.println("Setup DAC");
  // Set DAC resolution
  analogWriteResolution(DAC_RESOLUTION);
  analogReadResolution(ADC_RESOLUTION);
  // Magic https://github.com/adafruit/Adafruit_ZeroTimer/blob/master/examples/timers_callback/timers_callback.ino
  analogWrite(AUDIO_DAC, 128);
  analogWrite(STEP_DAC, 128);

  // Init SD Card
  sd.begin(SdSpiConfig(SD_CARD, SHARED_SPI, SD_SCK_MHZ(16)));

  // Load config
  config.loadConfig();
  
  // Load all patches into RAM
  patches.begin();

  // Set initial step direction based on loaded patch
  step.setStepDirection(patches.getCurrentPatch().stepDirection);
  
  // Setup the display
  Serial.println("Init Display");
  initDisplay();

  // Start our main audio callback
  //Serial.println("Enable Wave Callback");  
  TC.startTimer(MICROSECONDS / audio.sampleRate, waveStepCallback);
  
  //Serial.println("Setup Complete");
  wave.activate();
  currentModule = &wave;
  currentModuleId = SHOW_WAVE;
}

void loop() {
  // Read Frame CV, update wave if needed
  patches.updateWaveFrame();
  // Read V/Oct and Tune knob and set sample_rate
  audio.updatePitch();
  // Check for trigger and update step output
  step.update();
  // Run current module
  runModule();

  // Debug
  //debugPrintVOct();
  
}

void runModule() {
  returnedModuleId = currentModule->run();
  if(returnedModuleId != currentModuleId)
  {
    currentModuleId = returnedModuleId;
    switch (currentModuleId) {
      case MAIN_MENU:
        currentModule = &mainMenu;
        break;
      case SHOW_WAVE:
        currentModule = &wave;
        break;
      case BLANK:
        currentModule = &blank;
        break;
      default:
        currentModule = &mainMenu;
    }
    currentModule->activate();
  }
}

/*
void debugPrintVOct() {
  if(prevMillis + 1000 < millis())
  {
    Serial.print("Note: ");
    Serial.println(voct_note);
    Serial.print("Tune Note: ");
    Serial.println(tune_note);
    Serial.print("Effective Note: ");
    Serial.println(voct_note + tune_note);
    Serial.print("Pitch: ");
    Serial.println(pitch);
    Serial.print("Sample Rate: ");
    Serial.println(sample_rate);
    Serial.print("Raw V/Oct Value: ");
    Serial.println(ADC_MAX - analogRead(VOCT));
    Serial.print("Raw ADC Value: ");
    Serial.println(analogRead(VOCT));
    Serial.print("tuning_adj_value: ");
    Serial.println(tuning_adj_value);
    
    //Serial.print("Adjusted V/Oct Value: ");
    //Serial.println(adj_value);
    prevMillis = millis();  
  }   
}
*/


// Bootstrap the display
void initDisplay() {
  display.begin(SSD1306_SWITCHCAPVCC, SCREEN_ADDRESS);
  display.setRotation(0);
  display.setTextSize(1);      // Normal 1:1 pixel scale
  display.setTextColor(SSD1306_WHITE); // Draw white text
  display.cp437(true);         // Use full 256 char 'Code Page 437' font
  display.clearDisplay();
  display.display();
}

void waveStepCallback() {
  noInterrupts();

  // Step through our wave index, wrapping around when we reach the end.
  dacWaveIndex = ++dacWaveIndex % WAVE_SLOTS;

  // Magic writing to the DAC
  // See https://github.com/adafruit/Adafruit_ZeroTimer/blob/master/examples/timers_callback/timers_callback.ino
  while (DAC->SYNCBUSY.bit.DATA0);
  //DAC->DATA[0].reg = scaled_waves[current_wave][dac_wave_index];
  DAC->DATA[0].reg = patches.scaledWaves[patches.currentWave][dacWaveIndex];
  
  // Reset timer to new sample rate
  TC.setPeriod(MICROSECONDS / audio.sampleRate);

  interrupts();
}
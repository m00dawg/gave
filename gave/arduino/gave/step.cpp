// Step sequencer
#include "step.h"

Step::Step(Patches* patches) {
  this->patches = patches;
}

// Soon as we see a trigger, we need to advance the step
// but should only do so once per trigger
// (if trigger is held high, only step once)
void Step::update() {
  // If we read a trigger (it goes low), and the trigger flag was not already true,
  // set the trigger flag and update the step.
  if (!digitalRead(TRIGGER) && !trigger)
  {
    trigger = true;

    switch (direction) {
      // Forward
      case 'F':
        currentStep = ++currentStep % WAVE_SLOTS;
        break;
      // Backward (Reverse)
      case 'B':
        currentStep = --currentStep % WAVE_SLOTS;
        if(currentStep == 255)
          currentStep = WAVE_SLOTS;
        break;
      // Ping-Pong
      case 'P':
        // Forward
        if(pingPongDirection) {
          currentStep = ++currentStep;
          if(currentStep == WAVE_SLOTS)
            pingPongDirection = false;
        }
        // Backward
        else {
          currentStep = --currentStep;
          if(currentStep == 0)
            pingPongDirection = true;
        }
        break;
      // Random
      case 'R':
        currentStep = random(0, WAVE_SLOTS);
    }

    // Magic writing to the DAC
    // See https://github.com/adafruit/Adafruit_ZeroTimer/blob/master/examples/timers_callback/timers_callback.ino
    while (DAC->SYNCBUSY.bit.DATA1);
    DAC->DATA[1].reg = patches->scaledWaves[patches->currentWave][currentStep];
  }
  
  // If trigger is released (it goes high) and our flag was true, unset flag
  else if (digitalRead(TRIGGER) && trigger) {
    trigger = false;
  } 
}

void Step::setStepDirection(char direction) {
  this->direction = direction;
}

char Step::cycleStepDirection() {
  switch (direction) {
    // Forward
    case 'F':
      direction = 'B';
      break;
    case 'B':
      direction = 'P';
      break;
    case 'P':
      direction = 'R';
      break;
    case 'R':
      direction = 'F';
  }
  patches->updateStepDirection(direction);
  return direction;
}
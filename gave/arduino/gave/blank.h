/* Module which just blanks the display */
#include <Arduino.h>
#include <Adafruit_SSD1306.h>
#include <SimpleRotary.h>
#include "defines.h"
#include "module.h"

class Blank: public Module {
  public:
    Blank(Adafruit_SSD1306* display, SimpleRotary* upDownEncoder, SimpleRotary* leftRightEncoder);
    void activate();
    byte run();
};
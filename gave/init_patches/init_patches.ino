// Builds out an initial patch file for the M4's External Flash
#include <SPI.h>
#include <SdFat.h>
#include <Adafruit_SPIFlash.h>


// Waves
#define WAVE_SLOTS        32    // Number of samples per wave
#define WAVE_RESOLUTION   4     // Wave bits (4-bit)
#define WAVE_MAX          0xF   // Max wave range (4-bit)
#define WAVE_FRAMES       16    // Number of Frames (waves) per patch
#define NUM_PATCHES       16    // Number of Patches
#define PATCH_NAME_LENGTH 16    // Max name of patch (limited by display)
#define PATCHES_FILE_NAME "patches.bin"

Adafruit_FlashTransport_QSPI flashTransport;
Adafruit_SPIFlash flash(&flashTransport);
FatFileSystem fatfs;

File writeFile;

static char gb_squares_name[PATCH_NAME_LENGTH] = "GB Squares     ";
static char saws_name[PATCH_NAME_LENGTH] =       "Saws           ";

// Waveforms
static byte sawtooth_wave[WAVE_SLOTS] = {
  0xF, 0xF, 0xE, 0xE, 0xD, 0xD, 0xC, 0xC,
  0xB, 0xB, 0xA, 0xA, 0x9, 0x9, 0x8, 0x8,
  0x7, 0x7, 0x6, 0x6, 0x5, 0x5, 0x4, 0x4,
  0x3, 0x3, 0x2, 0x2, 0x1, 0x1, 0x0, 0x0
};

static byte square_12_wave[WAVE_SLOTS] = {
  0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,
  0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,
  0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,
  0x0, 0x0, 0x0, 0x0, 0xF, 0xF, 0xF, 0xF
};

static byte square_25_wave[WAVE_SLOTS] = {
  0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,
  0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,
  0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,
  0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF
};

static byte square_50_wave[WAVE_SLOTS] = {
  0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,
  0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,
  0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF,
  0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF
};



void setup() {
  // Initialize serial port and wait for it to open before continuing.
  Serial.begin(115200);
  while (!Serial) {
    delay(100);
  }
  Serial.println("Adafruit SPI Flash FatFs Full Usage Example");

  // Initialize flash library and check its chip ID.
  if (!flash.begin()) {
    Serial.println("Error, failed to initialize flash chip!");
    while(1) yield();
  }
  Serial.print("Flash chip JEDEC ID: 0x"); Serial.println(flash.getJEDECID(), HEX);

  // First call begin to mount the filesystem.  Check that it returns true
  // to make sure the filesystem was mounted.
  if (!fatfs.begin(&flash)) {
    Serial.println("Error, failed to mount newly formatted filesystem!");
    Serial.println("Was the flash chip formatted with the SdFat_format example?");
    while(1) yield();
  }
  Serial.println("Mounted filesystem!");

  if (!fatfs.remove(PATCHES_FILE_NAME)) {
    Serial.println("Error, couldn't delete file!");
    while(1) yield();
  }

  writeFile = fatfs.open(PATCHES_FILE_NAME, FILE_WRITE);
  if (!writeFile) {
    Serial.println("Error, failed to open file for writing!");
    while(1) yield();
  }
  Serial.print("Opened file ");
  Serial.print(PATCHES_FILE_NAME);
  Serial.println (" for writing...");

  writePatches();
  writeFile.close();
  
  Serial.print("Wrote to file ");
  Serial.println(PATCHES_FILE_NAME);
}

void loop () {
}

void writePatches () {  

  
   // Iterate through patches
  for (byte patch_count = 0; patch_count < NUM_PATCHES ; patch_count++)
  {
    // This is hacky but whatever
    // Create GB square waves
    if(patch_count == 4) {
      writeFile.write(gb_squares_name, PATCH_NAME_LENGTH);
          // Iterate through waves per patch
      writeFile.write(square_12_wave, WAVE_SLOTS);
      writeFile.write(square_12_wave, WAVE_SLOTS);
      writeFile.write(square_12_wave, WAVE_SLOTS);
      writeFile.write(square_12_wave, WAVE_SLOTS);
      writeFile.write(square_25_wave, WAVE_SLOTS);
      writeFile.write(square_25_wave, WAVE_SLOTS);
      writeFile.write(square_25_wave, WAVE_SLOTS);
      writeFile.write(square_25_wave, WAVE_SLOTS);
      writeFile.write(square_50_wave, WAVE_SLOTS);
      writeFile.write(square_50_wave, WAVE_SLOTS);
      writeFile.write(square_50_wave, WAVE_SLOTS);
      writeFile.write(square_50_wave, WAVE_SLOTS);
      writeFile.write(square_25_wave, WAVE_SLOTS);
      writeFile.write(square_25_wave, WAVE_SLOTS);
      writeFile.write(square_25_wave, WAVE_SLOTS);
      writeFile.write(square_25_wave, WAVE_SLOTS);
    }
    else {
      //Padding to 32
      //char char_patch_name[PATCH_NAME_LENGTH];
      //for (byte pad = 0 ; pad < PATCH_NAME_LENGTH ; pad++)
      //  char_patch_name[pad] = ' ';
        
       Serial.println("Writing patch ");
       //sprintf(char_patch_name, "%d", patch_count);
       Serial.println(saws_name);
       writeFile.write(saws_name, PATCH_NAME_LENGTH);
       // Iterate through waves per patch
       for (byte j = 0; j < WAVE_FRAMES ; j++)
       {
         Serial.print("Writing wave frame ");
         Serial.println(j);
         writeFile.write(sawtooth_wave, WAVE_SLOTS);
         /*
          // Make a squarewave
          if(patch_count != 0) {
            writeFile.write(square_50_wave, WAVE_SLOTS);
          }
          // Make a sawtooth
          else {
            writeFile.write(sawtooth_wave, WAVE_SLOTS);
          }
          */
       }
    }
  }
}

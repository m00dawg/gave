// Title: Gave
// Description: Implements a GameBoy WAVE style 4-bit/32 slot single wavetable
// Hardware: Adafruit ItsyBitsy M4
// Author: Tim Soderstrom
// Breadboard
// TBD
// Schematic:
// TBD

/* 
 *  Links:
 * https://learn.adafruit.com/introducing-adafruit-itsybitsy-m4/pinouts
 * https://github.com/ManojBR105/Teensy-4.0_digital_parallel_read-write
 * https://www.falstad.com/circuit/index.html
 * https://www.inspireAUDIO_DACoustics.com/en/MIDI_note_numbers_and_center_frequencies
 */

/* Pins Used:

 *  Pin A0: AUDIO_DAC
 *  Pin A1: Reserved (AUDIO_DAC2)
 *  Pin A2: V/Oct
 *  Pin A3: Tune
 *  Pin A4: Frame
 *  Pin 09: Encoder Up
 *  Pin 10: Encoder Down
 *  Pin 11: Button Menu
 *  Pin 12: Button Left
 *  Pin 13: Button Right
 *  
 *  Pin SDA: OLED SDA
 *  Pin SDL: OLED SDL
 *  

 */
 
// INCLUDES

#include <Arduino.h>
// Without this USB Serial doesn't seem to work right
#include <Adafruit_TinyUSB.h>
// Lib for turning off that awful RGB LED
#include <Adafruit_DotStar.h>
// Display Libs
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

// Interrupts
// This library caused pitch issues for some reason
//#include <SAMDTimerInterrupt.h>
// This library does not.
#include "SAMD51_InterruptTimer.h"

// Encoders & Buttons
#include <DailyStruggleButton.h>
#include <SimpleRotary.h>

// DEFINES
#include "defines.h"

// GLOBAL VARIABLES

// Display Variables
int static pixel_size_x = OLED_SCREEN_X / WAVE_SLOTS - 1;
int static pixel_size_y = OLED_SCREEN_Y / WAVE_MAX - 1;

// Raw note from V/Oct
float voct_note = 0;
// Raw note from tune
float tune_note = 0;
// Pitch, ultimately coming from v/oct plus tune
float pitch = 110;

// Naive tuning for the DAC. Sending a flaot to serial in changes this value.
// This seems to be closest at a -3 cents split.
float adj_value = 1.009811;

// Frame CV Value
int frame_cv = 0;

// Current sample rate to run the AUDIO_DAC at
// (pitch * 32 since there are 32 wave steps)
float sample_rate = 14080;  // Default to Middle-A

// Track where we are in the wave table for the DAC
volatile byte dac_wave_index = 0;

// Track where the user is in the wave index
byte user_wave_index = 0;

// For serial output debugging mostly
int prevMillis = 0;

// OBJECTS
// Display
Adafruit_SSD1306 display(OLED_SCREEN_X, OLED_SCREEN_Y, &Wire, OLED_RESET);

// That horrendously bright LED
Adafruit_DotStar rgbled(1, RGB_LED_DATAPIN, RGB_LED_CLOCKPIN, DOTSTAR_BRG);

// Control Surface
DailyStruggleButton buttonLeft;
DailyStruggleButton buttonRight;
DailyStruggleButton buttonMenu;
SimpleRotary upDownEncoder(ENCODER_UP, ENCODER_DOWN, 0);

// Local Includes
#include "patches.h"


// FUNCTIONS
void setup() {  
  Serial.begin(9600);
  delay(1000);
  
  Serial.println("Setup Pins");
  // Pins   
  pinMode(VOCT, INPUT_PULLUP);
  pinMode(TUNE, INPUT_PULLUP);

  buttonMenu.set(BUTTON_MENU, buttonMenuEvent, INT_PULL_UP);
  buttonLeft.set(BUTTON_LEFT, buttonLeftEvent, INT_PULL_UP);
  buttonRight.set(BUTTON_RIGHT, buttonRightEvent, INT_PULL_UP);
  buttonMenu.setDebounceTime(BUTTON_DEBOUNCE);
  buttonLeft.setDebounceTime(BUTTON_DEBOUNCE);
  buttonRight.setDebounceTime(BUTTON_DEBOUNCE);
  upDownEncoder.setDebounceDelay(ENCODER_DEBOUNCE);
  upDownEncoder.setErrorDelay(ENCODER_ERROR_DELAY);
  
  // Turn off the freakishly bright RGB LED
  rgbled.begin();
  rgbled.show(); // Initialize all pixels to 'off'

  Serial.println("Setup DAC");
  // Set DAC resolution
  analogWriteResolution(AUDIO_DAC_RESOLUTION);
  analogReadResolution(ADC_RESOLUTION);
  
  // Magic https://github.com/adafruit/Adafruit_ZeroTimer/blob/master/examples/timers_callback/timers_callback.ino
  analogWrite(AUDIO_DAC, 128);

  // Init flash and load all patches into RAM
  initFlash();
  loadAllPatches();

  Serial.println("Scale Waves");
  scaleWaves();

  // Setup the display
  Serial.println("Init Display");
  initDisplay();

  // Start our main audio callback
  Serial.println("Enable Wave Callback");
  TC.startTimer(MICROSECONDS / sample_rate, waveStepCallback);
  
  Serial.println("Setup Complete");
}

void loop() {
  // Read Frame CV, update wave is required
  checkWaveStep();
  // Read V/Oct and Tune knob and set sample_rate
  updatePitch();
  pollControlSurface();
  checkDisplay();
  debugPrintVOct();
  if (Serial.available() > 0) {
    adj_value = Serial.parseFloat();
    Serial.print("New adj: ");
    Serial.println(adj_value);
  }
}

void debugPrintVOct() {
  if(prevMillis + 1000 < millis())
  {
    Serial.print("Note: ");
    Serial.println(voct_note);
    Serial.print("Tune Note: ");
    Serial.println(tune_note);
    Serial.print("Effective Note: ");
    Serial.println(voct_note + tune_note);
    Serial.print("Pitch: ");
    Serial.println(pitch);
    Serial.print("Sample Rate: ");
    Serial.println(sample_rate);
    Serial.print("Raw V/Oct Value: ");
    Serial.println(ADC_MAX - analogRead(VOCT));
    Serial.print("Raw ADC Value: ");
    Serial.println(analogRead(VOCT));
    Serial.print("adj_value: ");
    Serial.println(adj_value);
    
    //Serial.print("Adjusted V/Oct Value: ");
    //Serial.println(adj_value);
    prevMillis = millis();  
  }   
}

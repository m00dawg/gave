#include <Arduino.h>
#include <SPI.h>
#include <SdFat.h>
#include <Adafruit_SPIFlash.h>
#include "defines.h"

// File I/O
extern Adafruit_FlashTransport_QSPI flashTransport;
extern FatFileSystem fatfs;
extern File patch_file;

// Struct for a single patch
struct Patch {
  char name[PATCH_NAME_LENGTH];
  byte frames[WAVE_FRAMES][WAVE_SLOTS];
};

// Pre-allocate space for patches in RAM
extern Patch patches[NUM_PATCHES];

// Current patch
extern byte current_patch;
// Current wave in patch
extern byte current_wave;

// RAM buffer for scaled waves from patch
extern volatile uint16_t scaled_waves[WAVE_FRAMES][WAVE_SLOTS];

// Scale the current wave for the current patch up to the AUDIO_DAC resolution 
// by bit-shifting and placing in our waves buffer.
void scaleWave(byte wave_number);

// Scale all waves in current patch up to AUDIO_DAC resolution. Generally should only
// need to call this if we switch patches.
void scaleWaves();

// Load all patches from flash into RAM
void loadAllPatches();

// Save all patches from RAM to flash
void saveAllPatches();

// Configure flash for reading and writing
void initFlash();

//GUI Interactions

#define NUM_PATCHES_ON_DISPLAY 6

// Display Modes Enum
#define DISPLAY_WAVE 0
#define DISPLAY_MAIN_MENU 1
#define DISPLAY_NAME_PATCH 2
#define DISPLAY_LOAD_PATCH 3
#define DISPLAY_SAVE_PATCH 4
#define DISPLAY_SCREENSAVER 5

// Main Menu Enum
#define MAIN_MENU_ENTRIES 5
#define MAIN_MENU_SHOW_WAVE 0
#define MAIN_MENU_NAME_PATCH 1
#define MAIN_MENU_LOAD_PATCH 2
#define MAIN_MENU_SAVE_PATCH 3
#define MAIN_MENU_SCREENSAVER 4


static String main_menu_list[MAIN_MENU_ENTRIES] = {
  "Show Wave",
  "Name Patch",
  "Load Patch",
  "Save All Patches",
  "Screensaver"
};


// Flag to indicate the display has been updated and we need to redraw it.
bool updateDisplay;

// Display Mode (menu vs wave, etc.)
byte display_mode = DISPLAY_MAIN_MENU;
byte main_menu_selection = MAIN_MENU_SHOW_WAVE;

// We can only display NUM_PATCHES_ON_DISPLAY at once
// This is how we page through the list.
byte patch_list_start = 0;
// The patch we have selected on the display
byte patch_list_selection = 0;
// The character of the patch name we have selected on the display
byte patch_name_selection = 0;

// Check on knobs and stuff
void buttonLeftEvent(byte buttonStatus) {
  switch (buttonStatus) {
    case onPress:
      if (display_mode == DISPLAY_WAVE) 
        user_wave_index = --user_wave_index % WAVE_SLOTS;
      if (display_mode == DISPLAY_LOAD_PATCH || display_mode == DISPLAY_SAVE_PATCH)
      {
        updateDisplay = true;
        display_mode = DISPLAY_MAIN_MENU;
      }
      if (display_mode == DISPLAY_NAME_PATCH) {
        updateDisplay = true;
        patch_name_selection = --patch_name_selection % PATCH_NAME_LENGTH;
      }
      Serial.println("Left Button");
      break;
  }
}

void buttonRightEvent(byte buttonStatus) {
  switch (buttonStatus) {
    case onPress:
      if (display_mode == DISPLAY_WAVE) {
        user_wave_index = ++user_wave_index % WAVE_SLOTS;
        Serial.println("Right Button");
      }
      if (display_mode == DISPLAY_NAME_PATCH) {
        updateDisplay = true;
        patch_name_selection = ++patch_name_selection % PATCH_NAME_LENGTH;
      }
      break;
  }
}

void buttonMenuEvent(byte buttonStatus) {
  switch (buttonStatus) {
    case onPress:
      // If we're not in a menu (so looking at a wave or with the screensaver going)
      // flip back to the main menu.
      if (display_mode == DISPLAY_WAVE || display_mode == DISPLAY_NAME_PATCH || display_mode == DISPLAY_SCREENSAVER)
        display_mode = DISPLAY_MAIN_MENU;
      // Otherwise if we're in the main menu, load up a new display depending on the
      // option desired.
      else if (display_mode == DISPLAY_MAIN_MENU) {
        switch(main_menu_selection) {
          case MAIN_MENU_SHOW_WAVE:
            display_mode = DISPLAY_WAVE;
            break;
          case MAIN_MENU_NAME_PATCH:
            display_mode = DISPLAY_NAME_PATCH;
            break;
          case MAIN_MENU_LOAD_PATCH:
            display_mode = DISPLAY_LOAD_PATCH;
            break;
          case MAIN_MENU_SAVE_PATCH:
            saveAllPatches();
            main_menu_selection = MAIN_MENU_SHOW_WAVE;
            display_mode = DISPLAY_MAIN_MENU;
            break;
          case MAIN_MENU_SCREENSAVER:
            display_mode = DISPLAY_SCREENSAVER;
            break;
          default:
            display_mode = DISPLAY_WAVE;
        }
      }
      // If we're in the load patch menu, pressing menu will load the requested patch into
      // the patch buffer (see patches.cpp)
      else if (display_mode == DISPLAY_LOAD_PATCH) {
        current_patch = patch_list_selection;
        scaleWaves();
        display_mode = DISPLAY_WAVE;
      }

      // If we're in the screensaver, pressing menu turns the screen on and opens the menu.
      else if (display_mode == DISPLAY_SCREENSAVER) {
        display_mode = DISPLAY_MAIN_MENU;
      }
      updateDisplay = true;
      Serial.println("Menu Button");
      break;
  }
}

void pollEncoder() {
  int position = upDownEncoder.rotate();
  // Up (Clockwise)
  if(position == 2)
  {
    if(display_mode == DISPLAY_WAVE)
    {
      patches[current_patch].frames[current_wave][user_wave_index] = ++patches[current_patch].frames[current_wave][user_wave_index] % (WAVE_MAX + 1);
      displayWaveView();
      scaleWave(current_wave);
      updateDisplay = true;
      Serial.println("Up");
    }
    else if(display_mode == DISPLAY_MAIN_MENU)
    {
      main_menu_selection = --main_menu_selection % MAIN_MENU_ENTRIES;
      updateDisplay = true;
      Serial.println("Move Up In Menu");
    }
    else if(display_mode == DISPLAY_LOAD_PATCH || display_mode == DISPLAY_SAVE_PATCH)
    {
      patch_list_selection = --patch_list_selection % NUM_PATCHES;
      updateDisplay = true;
      Serial.println("Move Up In Patch List");
    }
    else if (display_mode == DISPLAY_NAME_PATCH) {
      updateDisplay = true;
      patches[current_patch].name[patch_name_selection] = characterLookup(++patches[current_patch].name[patch_name_selection]);
    }
  }
  // Down (Counter-clockwise)
  else if(position == 1)
  {
    if(display_mode == DISPLAY_WAVE)
    {
      patches[current_patch].frames[current_wave][user_wave_index] = --patches[current_patch].frames[current_wave][user_wave_index] % (WAVE_MAX + 1);
      displayWaveView();
      scaleWave(current_wave);
      updateDisplay = true;
      Serial.println("Down");
    }
    else if(display_mode == DISPLAY_MAIN_MENU)
    {
      main_menu_selection = ++main_menu_selection % MAIN_MENU_ENTRIES;
      updateDisplay = true;
      Serial.println("Move Down In Menu");
    }
    else if(display_mode == DISPLAY_LOAD_PATCH || display_mode == DISPLAY_SAVE_PATCH)
    {
      patch_list_selection = ++patch_list_selection % NUM_PATCHES;
      updateDisplay = true;
      Serial.println("Move Down In Patch List");
    }
    else if (display_mode == DISPLAY_NAME_PATCH) {
      updateDisplay = true;
      patches[current_patch].name[patch_name_selection] = characterLookup(--patches[current_patch].name[patch_name_selection]);
    }
  }
}

// Bootstrap the display
void initDisplay() {
  display.begin(SSD1306_SWITCHCAPVCC, SCREEN_ADDRESS);
  display.setRotation(0);
  display.setTextSize(1);      // Normal 1:1 pixel scale
  display.setTextColor(SSD1306_WHITE); // Draw white text
  
  display.cp437(true);         // Use full 256 char 'Code Page 437' font
  updateDisplay = true;
  display_mode = DISPLAY_MAIN_MENU;
}

// Read Frame CV and update wave step if required
void checkWaveStep() {
  // Only grab positive CV values (need to make that configurable)
  if(analogRead(FRAME) <= FRAME_ADC_MAX) {
    frame_cv = map(analogRead(FRAME), FRAME_ADC_MAX, 16, 0, MAX_FRAME);
    if(frame_cv != current_wave) {
      Serial.println("Wave Frame Info: ");
      Serial.print("Raw Frame CV: ");
      Serial.println(analogRead(FRAME));
      Serial.print("Frame Value: ");
      Serial.println(frame_cv);
      current_wave = frame_cv;
      updateDisplay = true;
    }
  }
}

// Show the main menu, highlighting the current selected menu option.
void displayMainMenu() {
  display.clearDisplay();
  display.setCursor(0, 0);     // Start at top-left corner
  display.setTextColor(WHITE, BLACK);
  display.println("MENU\n----");
  //Serial.println(sizeof(main_menu_list));
  for (int i = 0; i < MAIN_MENU_ENTRIES; i++) {
    if(main_menu_selection == i)
      display.setTextColor(BLACK, WHITE);
    else
      display.setTextColor(WHITE, BLACK);
    display.println(main_menu_list[i]);
  }
  display.display();
}

// Name the current selected patch
// Left/Right move cursor
// Knob selects letter
// Menu Returns
void displayNamePatch() {
  display.clearDisplay();
  display.setCursor(0, 0);     // Start at top-left corner
  display.setTextColor(WHITE, BLACK);
  display.println("NAME PATCH\n----------");  
  
  for (int i = 0; i < PATCH_NAME_LENGTH; i++) {
    if(patch_name_selection == i)
      display.setTextColor(BLACK, WHITE);
    else
      display.setTextColor(WHITE, BLACK);
    display.print(patches[current_patch].name[i]);
  }
  display.setCursor(0, 32);
  display.setTextSize(3);
  display.print(patches[current_patch].name[patch_name_selection]);
  display.setTextSize(1);
  display.display();
}

// Draw the current wave view
void displayWaveView() {
  int x_pos = 0;
  int y_pos = 0;

  display.setCursor(0, 0);     // Start at top-left corner
  display.clearDisplay();

  display.print("Frame: ");
  display.println(current_wave);

  for (int i = 0; i < WAVE_SLOTS; i++) {
    x_pos = i * pixel_size_x;    
    y_pos = patches[current_patch].frames[current_wave][i] * pixel_size_y;
    for (int  x = 0 ; x < pixel_size_x; x++)
      for (int y = 0 ; y < pixel_size_y; y++) 
        // The weird OLED_SCREEN_Y - 1 - (y_pos + y) is to invert the displace
        // so "up" is actually up.
        display.drawPixel(x_pos + x, OLED_SCREEN_Y - 1 - (y_pos + y), SSD1306_WHITE);
  }
  display.display();
}

void displayPatches(bool save) {
  char hexNumber[1];
  display.setCursor(0, 0);
  display.clearDisplay();
  if(save)
    display.println("Save Patch To\n-------------");
  else
    display.println("Load Patch\n----------");
  for(int i = patch_list_start ; i < NUM_PATCHES_ON_DISPLAY + patch_list_start; i++)
  {
    if(patch_list_selection == i)
      display.setTextColor(BLACK, WHITE);
    else
      display.setTextColor(WHITE, BLACK);
    sprintf(hexNumber, "%X", i);
    display.print(hexNumber);
    display.print(":");
    display.println(patches[i].name);
  }
  display.display();
}

// Blank the screen
void displayScreensaver() {
  display.clearDisplay();
  display.display();
}



// Check if we need to update the display and if so, draw the relevant thing
void checkDisplay() {
  if(updateDisplay) {
    switch (display_mode) {
      case DISPLAY_MAIN_MENU:
        displayMainMenu(); 
        break;
      case DISPLAY_NAME_PATCH:
        displayNamePatch(); 
        break;
      case DISPLAY_LOAD_PATCH:
        displayPatches(0);
        break;
      case DISPLAY_SAVE_PATCH:
        displayPatches(1);
        break;
      case DISPLAY_SCREENSAVER:
        displayScreensaver();
        break;
      default:
        displayWaveView();
    }
    updateDisplay = false;
    Serial.println("Display Changed");
    Serial.print("Display Mode: ");
    Serial.println(display_mode);
  }
}


// Poll all the buttons and knobs and things.
void pollControlSurface() {
  buttonMenu.poll();
  buttonLeft.poll();
  buttonRight.poll();
  pollEncoder();
}

// Constrain character values within ones we want to allow in things like patch names
char characterLookup(byte char_value) {
  // This is how we wrap around
  if(char_value < CHAR_SPACE)
    return CHAR_LOWER_Z;
  if(char_value > CHAR_LOWER_Z)
    return CHAR_SPACE;

  // This is how we skip characters we don't want.
  // We went up from space, so hop to zero
  if(char_value == CHAR_BANG)
    return CHAR_ZERO;
  // We came down from zero to hop to space
  if(char_value == CHAR_SLASH)
    return CHAR_SPACE;
  // We went up to colon, so hop to A
  if(char_value == CHAR_COLON)
    return CHAR_UPPER_A;
  // We went down from A so hop to 9    
  if(char_value == CHAR_AT)
    return CHAR_NINE;
  // We went up to bracket so hop to a
  if(char_value == CHAR_LEFT_BRACKET)
    return CHAR_LOWER_A;
  // We went down from a so hop to Z
  if(char_value == CHAR_BACKTICK)
    return CHAR_UPPER_Z;
  // If we made it through that guantlet, return the character.
  return char_value;
}

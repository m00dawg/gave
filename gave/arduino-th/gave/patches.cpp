#include "patches.h"

// File I/O
Adafruit_FlashTransport_QSPI flashTransport;
Adafruit_SPIFlash flash(&flashTransport);
FatFileSystem fatfs;
File patch_file;

// Pre-allocate space for patches in RAM
Patch patches[NUM_PATCHES];

// Current patch
byte current_patch = 0;
// Current wave in patch
byte current_wave = 0;

// RAM buffer for scaled waves from patch
volatile uint16_t scaled_waves[WAVE_FRAMES][WAVE_SLOTS];

// Scale the current wave for the current patch up to the AUDIO_DAC resolution
// by bit-shifting and placing in our waves buffer.
void scaleWave(byte wave_number) {
  for (byte i = 0; i < WAVE_SLOTS; i++) {
    scaled_waves[wave_number][i] = map(patches[current_patch].frames[wave_number][i], 0, WAVE_MAX, 0, AUDIO_DAC_MAX);
  }
}

// Scale all waves in current patch up to AUDIO_DAC resolution. Generally should only
// need to call this if we switch patches.
void scaleWaves() {
  for (byte i = 0; i < WAVE_FRAMES; i++) {
    scaleWave(i);
  }
}

// Load all patches from flash into RAM
void loadAllPatches() {
  char c;
  Serial.println("Loading all patches");
  
  patch_file = fatfs.open(PATCHES_FILE_NAME, FILE_READ);
  if (!patch_file) {
    Serial.println("Error, failed to open file reading!");
    while(1) yield();
  }

  while (patch_file.available()) {
    // Iterate through each patch
    for (byte patch_count = 0; patch_count < NUM_PATCHES ; patch_count++) {
      // Read the patch name
      for (byte name_count = 0 ; name_count < PATCH_NAME_LENGTH ; name_count++) {
        c = patch_file.read();
        patches[patch_count].name[name_count] = c;
      }
      Serial.print("Loading patch: ");
      Serial.println(patches[patch_count].name);
      // Then the waves
      for (byte wave_frame_count = 0; wave_frame_count < WAVE_FRAMES ; wave_frame_count++) {
        Serial.print("Loading Frame: ");
        Serial.println(wave_frame_count);
        Serial.print("Wave: ");
        for (byte wave_slot_count = 0 ; wave_slot_count < WAVE_SLOTS ; wave_slot_count++) {
          c = patch_file.read();
          patches[patch_count].frames[wave_frame_count][wave_slot_count] = c;
        }
      }
    }
  }
  patch_file.close();
}

// Save all patches from RAM to flash
// This is less refined, ideally saving a single patch might make more sense
// from the standpoint of wear, but this is what we got for now.
void saveAllPatches() {
  // Erase exiting patch file
  Serial.println("Erasing Patch File");
  if (!fatfs.remove(PATCHES_FILE_NAME)) {
    Serial.println("Error, couldn't delete file!");
    while(1) yield();
  }
  
  // Open file for writing
  patch_file = fatfs.open(PATCHES_FILE_NAME, FILE_WRITE);
  if (!patch_file) {
    Serial.println("Error, failed to open file for writing!");
    while(1) yield();
  }
  Serial.print("Opened file ");
  Serial.print(PATCHES_FILE_NAME);
  Serial.println (" for writing...");

   // Iterate through patches
  for (byte patch_count = 0; patch_count < NUM_PATCHES ; patch_count++)
  {
     Serial.print("Writing patch ");
     Serial.println(patches[patch_count].name);
     patch_file.write(patches[patch_count].name, PATCH_NAME_LENGTH);
     // Iterate through waves per patch
     for (byte frame_count = 0; frame_count < WAVE_FRAMES ; frame_count++)
     {
       Serial.print("Writing wave frame ");
       Serial.println(frame_count);
       patch_file.write(patches[patch_count].frames[frame_count], WAVE_SLOTS);
     }
  }
  patch_file.close();
}


void initFlash() {
  Serial.println("Adafruit SPI Flash FatFs Full Usage Example");

  // Initialize flash library and check its chip ID.
  if (!flash.begin()) {
    Serial.println("Error, failed to initialize flash chip!");
    while(1) yield();
  }
  Serial.print("Flash chip JEDEC ID: 0x");
  Serial.println(flash.getJEDECID(), HEX);

  // First call begin to mount the filesystem.  Check that it returns true
  // to make sure the filesystem was mounted.
  if (!fatfs.begin(&flash)) {
    Serial.println("Error, failed to mount newly formatted filesystem!");
    Serial.println("Was the flash chip formatted with the SdFat_format example?");
    while(1) yield();
  }
  Serial.println("Mounted filesystem!");
}

// Builds out an initial patch file for the M4's External Flash
#include <SPI.h>
#include <SdFat.h>
#include <Adafruit_SPIFlash.h>


// Waves
#define WAVE_SLOTS        32    // Number of samples per wave
#define WAVE_RESOLUTION   4     // Wave bits (4-bit)
#define WAVE_MAX          0xF   // Max wave range (4-bit)
#define WAVE_FRAMES       16    // Number of Frames (waves) per patch
#define NUM_PATCHES       16    // Number of Patches
#define PATCH_NAME_LENGTH 16    // Max name of patch (limited by display)
#define PATCHES_FILE_NAME "patches.bin"

Adafruit_FlashTransport_QSPI flashTransport;
Adafruit_SPIFlash flash(&flashTransport);
FatFileSystem fatfs;

// Struct for a single patch
struct Patch {
  char name[PATCH_NAME_LENGTH];
  byte frames[WAVE_FRAMES][WAVE_SLOTS];
};

// Pre-allocate space for patches in RAM
Patch patches[NUM_PATCHES];

File readFile;

void setup() {
  // Initialize serial port and wait for it to open before continuing.
  Serial.begin(115200);
  while (!Serial) {
    delay(100);
  }
  Serial.println("Adafruit SPI Flash FatFs Full Usage Example");

  // Initialize flash library and check its chip ID.
  if (!flash.begin()) {
    Serial.println("Error, failed to initialize flash chip!");
    while(1) yield();
  }
  Serial.print("Flash chip JEDEC ID: 0x"); Serial.println(flash.getJEDECID(), HEX);

  // First call begin to mount the filesystem.  Check that it returns true
  // to make sure the filesystem was mounted.
  if (!fatfs.begin(&flash)) {
    Serial.println("Error, failed to mount newly formatted filesystem!");
    Serial.println("Was the flash chip formatted with the SdFat_format example?");
    while(1) yield();
  }
  Serial.println("Mounted filesystem!");


  readFile = fatfs.open(PATCHES_FILE_NAME, FILE_READ);
  if (!readFile) {
    Serial.println("Error, failed to open file reading!");
    while(1) yield();
  }

  Serial.println("Entire contents of file:");
  readPatches();
}


void loop () {
}

void readPatches () {
  byte new_line_count = 0;
  char c;
  char hexVal[9];
  while (readFile.available()) {
    // Iterate through each patch
    for (byte i = 0; i < NUM_PATCHES ; i++) {
      // Print the name first
      for (byte j = 0 ; j < PATCH_NAME_LENGTH ; j++) {
        c = readFile.read();
        Serial.print(c);
      }
      Serial.println();
      
      // Then print the waves in hex
      for (byte k = 0; k < WAVE_FRAMES ; k++) {
        Serial.print("Wave Frame: ");
        Serial.println(k);
        for (byte l = 1 ; l <= WAVE_SLOTS ; l++) {
          c = readFile.read();
          sprintf(hexVal, "%02X ", c);
          Serial.print(hexVal);
          if(l % 8 == 0)
            Serial.println();
        }
        Serial.println();
      }
      Serial.println();
    }
  }
}
